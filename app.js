var sentry = require('@sentry/node');

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
//tentative de mise en place du handler sentry
var app = express();

//tentative de mise en place du handler sentry
sentry.init({ dsn: "https://glet_6188ada5cbbb7447f48cc39d4a6d119e@observe.gitlab.com:443/errortracking/api/v1/projects/53757294" });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get("/debug-sentry", function mainHandler(req, res) {
  throw new Error("My first Sentry error!");
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
app.use(sentry.Handlers.errorHandler());

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = `${res.sentry} - ${err.message}`;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
